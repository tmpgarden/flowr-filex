FROM node:alpine

ENV DATA_DIR=/opt/app/data

RUN mkdir -p /opt/app/src /opt/app/data
WORKDIR /opt/app/src
COPY package.json package-lock.json /opt/app/src/
RUN npm install --silent
COPY . /opt/app/src/

VOLUME /opt/app/data
EXPOSE 3000

CMD [ "npm", "start" ]
