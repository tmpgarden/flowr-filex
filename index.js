var fse = require('fs-extra')
var express = require('express');
var cors = require('cors');
var multer  = require('multer')

var filexDir = process.env.DATA_DIR || __dirname + '/uploads';
if(filexDir.charAt(filexDir.length-1)!=='/'){
  filexDir = filexDir + '/';
}
fse.ensureDirSync(filexDir);

var upload = multer({ dest: filexDir})

var app = express();
app.use(cors());
app.use(express.static('public'));

var server1 = app.listen(process.env.PORT || 3000, function () {
  var host = server1.address().address;
  var port = server1.address().port;
  console.log('flowr-filex listening at http://%s:%s', host, port);
});

var host = process.env.HOST || 'http://localhost:3000'
if(host.charAt(host.length-1)!=='/'){
  host = host + '/';
}

app.post('/', upload.any(), function (req, res) {
  var keys = []
  for (var i = 0; i < req.files.length; i++) {
    keys.push(host + req.files[i].filename)
  }

  return res.json(keys);
})

app.get('/:key', function(req, res){
  res.sendFile(filexDir + req.params.key);
});
